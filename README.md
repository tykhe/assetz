# Stock price prediction using Random Forest

## Requirements

- Python3.6
- Virtualenv
- All required libraries should be listed in `setup.py` file.

## Installation

```
virtualenv .venv
source .venv/bin/activate
pip install .
```

This will install all requirements into virtual environment, to use it you will 
need to activate it first:

```
source .venv/bin/activate
```

## How to use

Run `python predict <SYMBOL>`.

There are also a few optional parameters:

- `--period` (default: 90) prediction period
- `--go_back` (default: 0) go back in time N candlesticks
- `--verbose` which will output more information when it runs.
- `--offline` won't try to download data from the sources

## Changing settings

Change `settings.py` file, there are two settings variables which are params used for the
relevant methods.

### SPLIT_PARAMS

Used for `train_test_split` all params available in the documentation: 
http://scikit-learn.org/stable/modules/generated/sklearn.model_selection.train_test_split.html

### CLASSIFIER_PARAMS

RandomForestClassifier parameters

All available parameters listed in the documentation:
http://scikit-learn.org/stable/modules/generated/sklearn.ensemble.RandomForestClassifier.html#sklearn.ensemble.RandomForestClassifier

### Indicators

There are two libraries used for generating technical indicators data: stocksstats and ti.

You can change the indicators using `DEFAULT_INDICATORS` settings variable, just prefix the indicator
name with either `stockstats:` or `ti:` list of `ti` indicators is available in `ti.py` and list of `stockstats`
is available in the library repo https://github.com/jealous/stockstats/blob/master/stockstats.py

You'd also need to change `PREDICT_FEATURES` in settings file if you've changed `DEFAULT_INDICATORS`.
`PREDICT_FEATURES` is used to filter out any intermediate variables set by technical indicators libraries.

## Modifying code to allow new sources and intervals

At the moment the code only uses a single data source and interval, daily data from IEX.

In order to run the algorithm on a data from different source, following changes required:

- Adding new parameters in `__main__.py` file:
    - `--interval`, default `1d`
    - `--source`, with default `iex`
- Passing these params to `process` function of `randomforest.py`
- Add new file in loader package: `binance.py` and add functions to connect to binance and code to save data locally
  as JSON files, sample code can be found in `loader/iex.py`.

  This functionality can use `python-binance` package or use REST API directly.
  REST API might be the suggested (lightweight) choice as the URL can be easily constructed and historical data can
  be loaded via recursive function (by passing endTime as the URL parameter).

  This function should also change the data, as binance API returns data in a list of list, whereas IEX at the moment
  returns a list of dictionaries (https://api.iextrading.com/1.0/stock/aapl/chart/5y), ideally binance wrapper
  should convert the data to the same format as IEX does, so it can be converted to the pandas DataFrame for the
  future use in technical indicators generation and algorithms.
- Changing `get_data` function from `data.py` to so it can load different data sources, i.e. passing `source`
  and `interval` parameters, and choosing the correct loader function depending on the exchange and interval.
- Optionally, but recommended, adding source and interval values in the output info after the prediction when
  `--verbose` option is used.

