import concurrent.futures
import glob
import os
import pandas as pd
import numpy as np
import datetime
from assetpredict.randomforest import process
from assetpredict.settings import CACHE_DIR

pd.options.mode.chained_assignment = None

'''
How to fetch symbols from FTSE
curl "http://www.londonstockexchange.com/ \
    exchange/prices-and-markets/stocks/indices/summary/summary-indices-constituents.html?index=UKX&page=1" |\
    grep -Po 'scope="row" class="name">\K[^<]*' | cat >> FTSE100.txt

How to fetch symbols from NASDAQ
NASDAQ
curl "https://www.nasdaq.com/screening/companies-by-industry.aspx?exchange=NASDAQ&page=2" | \
    grep -Po 'stockticker=\K[^"]*' | cat >> NASDAQ.txt

NASDAQ100
curl "https://www.nasdaq.com/quotes/nasdaq-100-stocks.aspx" | grep -Po '\["\K[^"]*' | cat >> NASDAQ100.txt

NASDAQ-FIN100
curl "https://www.nasdaq.com/quotes/nasdaq-financial-100-stocks.aspx" | grep -Po '\["\K[^"]*' | cat >> NASDAQ100.txt

DOWIA
curl "https://www.nasdaq.com/quotes/djia-stocks.aspx" | grep -Po '\["\K[^"]*' | cat >> NASDAQ100.txt

'''


# if __name__ == "__main__":
def main():
    # portfolio = './in/test-binance.txt'
    # portfolio = './in/test-portfolio.txt'
    # portfolio = './in/basson-portfolio-01-US.txt'
    portfolio = './in/NASDAQ100.txt'
    # portfolio = 'in/FTSE250.txt'
    interval='5y'
    candle='1d'
    # period = 21
    # go_back = 83
    us_holidays = ['2018-01-01', '2018-01-15', '2018-02-19', '2018-03-30', '2018-05-28',
                   '2018-07-04', '2018-09-03', '2018-11-22', '2018-12-25']
    us_weekmask = '1111100'
    go_back = np.busday_count('2018-06-29', '2018-11-27', weekmask=us_weekmask, holidays=us_holidays,)
    lower = 0.25
    upper = 0.75
    friction = 0.0
    periods = [np.busday_count('2018-06-29', '2018-07-31', weekmask=us_weekmask, holidays=us_holidays),
               np.busday_count('2018-06-29', '2018-08-31', weekmask=us_weekmask, holidays=us_holidays),
               np.busday_count('2018-06-29', '2018-09-30', weekmask=us_weekmask, holidays=us_holidays),
               np.busday_count('2018-06-29', '2018-10-31', weekmask=us_weekmask, holidays=us_holidays),
               np.busday_count('2018-06-29', '2018-11-26', weekmask=us_weekmask, holidays=us_holidays),
               ]
    # for period in [21, 42, 63, 84, 105, 126]:
    for period in periods:
        # all_symbols('iex', interval, candle, period, go_back, lower, upper, None, friction,)
        # all_symbols('iex', interval, candle, period, go_back, lower, upper, 'MinMaxScaler', friction, )
        # all_symbols('iex', interval, candle, period, go_back, lower, upper, 'RobustScaler', friction, )
        my_portfolio(portfolio, 'iex', interval, candle, period, go_back, lower, upper, None, friction, )
        # my_portfolio(portfolio, 'iex', interval, candle, period, go_back, lower, upper, 'MinMaxScaler', friction, )
        # my_portfolio(portfolio, 'iex', interval, candle, period, go_back, lower, upper, 'RobustScaler', friction, )
    go_back = 0
    periods = [np.busday_count('2018-11-26', '2018-12-31', weekmask=us_weekmask, holidays=us_holidays),
               np.busday_count('2018-11-26', '2019-01-31', weekmask=us_weekmask, holidays=us_holidays),
               np.busday_count('2018-11-26', '2019-02-28', weekmask=us_weekmask, holidays=us_holidays),
               np.busday_count('2018-11-26', '2019-03-31', weekmask=us_weekmask, holidays=us_holidays),
               np.busday_count('2018-11-26', '2019-04-30', weekmask=us_weekmask, holidays=us_holidays),
               np.busday_count('2018-11-26', '2019-05-31', weekmask=us_weekmask, holidays=us_holidays),
               ]
    # for period in [21, 42, 63, 84, 105, 126]:
    for period in periods:
        # all_symbols('iex', interval, candle, period, go_back, lower, upper, None, friction,)
        # all_symbols('iex', interval, candle, period, go_back, lower, upper, 'MinMaxScaler', friction, )
        # all_symbols('iex', interval, candle, period, go_back, lower, upper, 'RobustScaler', friction, )
        my_portfolio(portfolio, 'iex', interval, candle, period, go_back, lower, upper, None, friction, )
        # my_portfolio(portfolio, 'iex', interval, candle, period, go_back, lower, upper, 'MinMaxScaler', friction, )
        # my_portfolio(portfolio, 'iex', interval, candle, period, go_back, lower, upper, 'RobustScaler', friction, )


def my_portfolio(portfolio, source='iex', interval='5y', candle='1d', period=88, go_back=0, lower=0.25, upper=0.75, scaler=None, friction=5.0, offline=False):
    """
    For each asset in 'portfolio', predict the outcome in 'period' candles with a signal (Sell strong, Sell, Buy, Buy strong).
    :param portfolio: a path to the text file containing symbols of assets in a portfolio, one per line.
    :param source: the source for symbol data used to calculate prediction features (Technical Indicators). Default = 'iex'.
    :param period: The prediction period in candles (default=88).
    :param go_back: Used for back-testing, the number of candles to go back in the historical data (default=0).
    :param lower: the lower quantile (default=0.25) used to generate the 'Sell strong' signal (-2.0).
    :param upper: the upper quantile (default=0.75) used to generate 'Buy strong' signal (2.0).
    :param scaler: one of None, MinMaxScaler, RobustScaler (default=None).
    :param friction: The simulated cost of trading taking into account expected losses, commissions, and slippage (default=5.0%).
    :param offline: True if running only on cached historical data, False (default) otherwise.
    :return: Nothing.
    """
    symbols = np.loadtxt(portfolio, dtype=np.str, )
    print(symbols)

    r_arr = []
    for symbol in symbols:
        print('Symbol: {0}'.format(symbol))
        err, d = process(symbol=symbol,
                         source=source,
                         interval=interval,
                         candle=candle,
                         period=period,
                         go_back=go_back,
                         lower=lower,
                         upper=upper,
                         scaler=scaler,
                         friction=friction,
                         offline=offline)
        if not err:
            if d:
                r_arr.append(d)

    df = pd.DataFrame.from_dict(r_arr)
    # df.set_index('symbol')
    print(df.shape)

    today = datetime.datetime.today().strftime('%Y-%m-%d')
    df.to_csv('./out/nasdaq100_5sigs_rf/predictions_{0}_{1}_{2}_{3}_{4}_{5}_{6}_{7}_{8}.csv'
              .format(source, interval, candle, scaler, lower, upper, today, period, go_back))


def all_symbols(source='iex', interval='5y', candle='1d', period=88, go_back=0, lower=0.25, upper=0.75,
                scaler=None, friction=5.0, offline=False):
    """

    :param source:
    :param period:
    :param go_back:
    :param scaler:
    :param offline:
    :return:
    """
    r_arr = []
    data_dir = os.path.join(CACHE_DIR, source)
    for item in os.scandir(data_dir):
        if item.is_file() and item.name.endswith('.json.gz'):
            if source == 'iex':
                stock_symbol = item.name[9:-8]
            elif source == 'binance':
                stock_symbol = item.name[12:-8]
            print(stock_symbol)
            err, d = process(symbol=stock_symbol,
                             source=source,
                             interval=interval,
                             candle=candle,
                             period=period,
                             go_back=go_back,
                             scaler=scaler,
                             friction=friction,
                             offline=offline)
            if not err:
                if d:
                    r_arr.append(d)

    df = pd.DataFrame.from_dict(r_arr)
    today = datetime.datetime.today().strftime('%Y-%m-%d')
    df.to_csv('./out/predictions_{0}_sbbsss_{1}_{2}_{3}_{4}.csv'
              .format(source, scaler, today, period, go_back))


main()
