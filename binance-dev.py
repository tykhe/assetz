import ccxt
import requests
import json
import os
import time
import datetime
import gzip
import pandas as pd
from predict.settings import CACHE_DIR


def stuff():
    # get_symbols()
    # end = time.time()
    # start = end - (1000 * 60 * 60 * 24)
    # print(end, start)
    start = 0.0
    end = 0.0
    today = datetime.datetime.today().timestamp()
    now = time.time() * 1000
    print(now, today)
    data = get_data('SKY/BNB', '1h', start, end, )
    print(len(data), data)
    # data = requests.get(f'https://api.binance.com/api/v1/exchangeInfo').json()
    # print(data)
    # for symbol in data['symbols']:
    #     print(symbol)


def get_symbols():
    symbols = []
    data = requests.get(f'https://api.binance.com/api/v1/exchangeInfo').json()
    for symbol in data['symbols']:
        symbols.append(symbol['symbol'])
    # print(symbols)
    df = pd.DataFrame(symbols, columns=('symbols', ))
    df.to_csv('./data_cache/binance/markets.csv')


def get_data(symbol, interval, start, end, ):
    try:
        print(symbol)
        binance = ccxt.binance()
        binance.load_markets()
        print(binance.timeframes)
        assert interval in binance.timeframes
    #   print(binance, binance.markets[symbol], binance.timeframes, )
        if binance.has['fetchOHLCV']:
            # data = binance.fetch_ohlcv(symbol, interval, 1523664000000, 10, )
            data = binance.fetch_ohlcv(symbol, interval, since=1516100000000, limit=10000, )
            end_time = data[-1][0]
            start_time = data[-len(data)][0]
            print(len(data), start_time, end_time, end_time - start_time,)
            # while True:
            #     time.sleep(binance.rateLimit / 1000)
            #     data = binance.fetch_ohlcv(symbol, interval, )
            #     end_time = data[-1][0]
            #     start_time = data[-len(data)][0]
            #     print(start_time, end_time, end_time - start_time,)
        return data
        #     for symbl in binance.markets:
        #         time.sleep(binance.rateLimit / 1000)
        #         print(symbl, binance.fetch_ohlcv(symbl, interval))
        # request_str = 'https://api.binance.com/api/v1/klines?symbol={0}&interval={1}&startTime={2}&endTime={3}'\
        #     .format(symbol.upper(), interval.lower(), start, end, )
        # print(request_str)
        # data = []
        # data = requests.get(request_str).json()
        # print(data)
    except AssertionError:
        print('Assertion error!')


stuff()
