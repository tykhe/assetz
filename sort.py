import pandas as pd
import numpy as np


def main():
    sort_portfolio('./in/my-portfolio.csv')


def sort_portfolio(pf):
    stocks = np.loadtxt(pf, dtype=np.str, )
    print(stocks.shape)
    stocks.sort()
    np.savetxt(pf, stocks, fmt='%s')


def sort():
    df = pd.read_csv('all_predictions_20180515.csv')
    df.sort_values(['signal_22', 'signal_44', 'signal_66', 'signal_88', 'signal_110', 'signal_132', 'symbol',], inplace=True, )
    df.to_csv('all_predictions_20180515.csv')
    df_buy = df.loc[(df['signal_22'] == 'Buy') &
                    (df['signal_44'] == 'Buy') &
                    (df['signal_66'] == 'Buy') &
                    (df['signal_88'] == 'Buy') &
                    (df['signal_110'] == 'Buy') &
                    (df['signal_132'] == 'Buy')]
    df_buy = df_buy.sort_values(['signal_22', 'signal_44', 'signal_66', 'signal_88', 'signal_110', 'signal_132', 'symbol',], )
    df_buy.to_csv('predictions_iex_all_buy_20180515.csv')
    df_sell = df.loc[(df['signal_22'] == 'Sell') &
                    (df['signal_44'] == 'Sell') &
                    (df['signal_66'] == 'Sell') &
                    (df['signal_88'] == 'Sell') &
                    (df['signal_110'] == 'Sell') &
                    (df['signal_132'] == 'Sell')]
    df_sell = df_sell.sort_values(['signal_22', 'signal_44', 'signal_66', 'signal_88', 'signal_110', 'signal_132', 'symbol',], )
    df_sell.to_csv('predictions_iex_all_sell_20180515.csv')


main()