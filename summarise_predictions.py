import pandas as pd
import numpy as np


def main():
    source = 'iex'
    interval = '5y'
    candle = '1d'
    lower = 0.25
    upper = 0.75
    date = '2018-10-19'
    period = 126
    go_back = 126
    select_stocks('./out/predictions_iex_{0}_{1}_None_{2}_{3}_{4}_{5}_{6}.csv'
                .format(interval, candle, lower, upper, date, period, go_back),
                source, interval, candle, date, lower, upper, period, go_back, )
    scaler = 'MinMaxScaler'
    select_stocks('./out/predictions_iex_{0}_{1}_MinMaxScaler_{2}_{3}_{4}_{5}_{6}.csv'
                .format(interval, candle, lower, upper, date, period, go_back),
                source, interval, candle, date, scaler, lower, upper, period, go_back, )
    scaler = 'RobustScaler'
    select_stocks('./out/predictions_iex_{0}_{1}_RobustScaler_{2}_{3}_{4}_{5}_{6}.csv'
                .format(interval, candle, lower, upper, date, period, go_back),
                source, interval, candle, date, scaler, lower, upper, period, go_back, )


def invest(accuracy, signal):
    return_val = 0.0
    if accuracy >= 0.8:
        if signal == -2.0:
            return_val = -500.0
        elif signal == 2.0:
            return_val = 500.0
    return return_val


def select_stocks(predictions, source, interval, candle, date, scaler=None,
                lower=0.25, upper=0.75, period=22, go_back=0, ):
    df = pd.read_csv(predictions)
    # df['potential_profit'] = np.subtract(df['(-1) close'], df['close'])
    # df['potential_profit_%'] = (df['potential_profit'] / df['close']) * 100.0
    df['investment'] = df[['accuracy', 'prediction']].apply(lambda x: invest(*x), axis=1)
    # df['invt_profit'] = (df['potential_profit_%'] * df['investment']) / 100
    df[['accuracy', 'candle', 'close', 'date', 'features', 'interval', 'precision',
        'prediction', 'recall', 'shape', 'signal', 'source', 'symbol', 'investment', ]]\
        .to_csv(predictions)
    output = './out/predictions_iex_{0}_{1}_{2}_{3}_{4}_{5}_{6}_{7}_summary.txt' \
        .format(source, interval, candle, date, scaler, lower, upper, period, go_back, )
    count = df['investment'].astype(bool).sum()
    with open(output, 'w') as file:
        # file.write('Profit = ${:,.2f}'.format(df['invt_profit'].sum()))
        file.write('Technical indicators suggest investing ${:,.2f} in {} stocks.\n'.format(count * 500, count))
        symbols = df.loc[(df.investment == 500.0) | (df.investment == -500.0) & (df.accuracy >= 0.85), ['symbol', 'signal', 'close', ]]
        for index, row in symbols.iterrows():
            file.write('{}, {} at ${:,.2f}\n'.format(row['symbol'], row['signal'], row['close']))


main()
