import concurrent.futures
import glob
import os
import pandas as pd
from assetpredict.randomforest import process
from assetpredict.settings import CACHE_DIR


def main():
#     predict(22)
#     predict(44)
#     predict(66)
#     predict(88)
#     predict(110)
#     predict(132)
    predict(21, 126)
    predict(42, 126)
    predict(63, 126)
    predict(84, 126)
    predict(105, 126)
    predict(1126, 126)
    compare('./out/predictions_iex_sbbsss_2018-10-14_')


# compare across a number of periods
def compare(prefix):
    df22 = pd.read_csv('{0}22_0.csv'.format(prefix))
    df44 = pd.read_csv('{0}44_0.csv'.format(prefix))
    df66 = pd.read_csv('{0}66_0.csv'.format(prefix))
    df88 = pd.read_csv('{0}88_0.csv'.format(prefix))
    df110 = pd.read_csv('{0}110_0.csv'.format(prefix))
    df132 = pd.read_csv('{0}132_0.csv'.format(prefix))
    df = df22[['symbol', 'close', 'date', 'signal']]\
        .join(df44['signal'], rsuffix='_44', )\
        .join(df66['signal'], rsuffix='_66', )\
        .join(df88['signal'], rsuffix='_88')\
        .join(df110['signal'], rsuffix='_110', )\
        .join(df132['signal'], rsuffix='_132')
    df = df.sort_values(by=['signal', 'signal_44', 'signal_66', 'signal_66', 'signal_88', 'signal_110', 'signal_132'], )
    df.to_csv('{0}_summary.csv'.format(prefix))


def predict(period=66, go_back=0, source='iex', interval='5y', candle='1d', lower=0.25, upper=0.75,
            scaler=None, friction=5.0, optimise=False, offline=False):
    r_arr = []
    # stocks = []
    for item in os.scandir('./data_cache/iex'):
        if item.is_file() and item.name.endswith('.json.gz'):
            stock_symbol = item.name[9:-8]
            print(stock_symbol)
            # stocks.append(item.name[9:-8])

    # with concurrent.futures.ProcessPoolExecutor() as executor:
    #     err, d = executor.map(process, stocks,
    #                  [source] * len(stocks),
    #                  [interval] * len(stocks),
    #                  [candle] * len(stocks),
    #                  [period] * len(stocks),
    #                  [go_back] * len(stocks),
    #                  [lower] * len(stocks),
    #                  [upper] * len(stocks),
    #                  [scaler] * len(stocks),
    #                  [friction] * len(stocks),
    #                  [optimise] * len(stocks),
    #                  [offline] * len(stocks),)

            err, d = process(stock_symbol,
                             source=source,
                             interval=interval,
                             candle=candle,
                             period=period,
                             go_back=go_back,
                             lower=lower,
                             upper=upper,
                             scaler=scaler,
                             friction=friction,
                             optimise=optimise,
                             offline=offline,)
            if not err:
                if d:
                    r_arr.append(d)

    df = pd.DataFrame.from_dict(r_arr)
    df.set_index('symbol')
    df.to_csv('predictions_iex_20180515_{0}_{1}_{2}.csv'.format(period, go_back, offline))


main()
