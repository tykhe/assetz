import pandas as pd
import numpy as np
import json


sharpe_portfolio = './out/basson_us_2sigs_rf/portfolio.json'
with open(sharpe_portfolio) as f:
    weights = json.load(f)
    f.close()


def main():
    source = 'iex'
    interval = '5y'
    candle = '1d'
    lower = 0.25
    upper = 0.75
    date = '2018-11-06'
    go_back = 89
    for period in [21, 44, 64, 86]:
    # for period in [22]:
        calc_profit('./out/basson_us_2sigs_rf/predictions_iex_{0}_{1}_None_{2}_{3}_{4}_{5}_{6}.csv'
                    .format(interval, candle, lower, upper, date, period, go_back),
                    source, interval, candle, date, lower, upper, period, go_back, )
        scaler = 'MinMaxScaler'
        calc_profit('./out/basson_us_2sigs_rf/predictions_iex_{0}_{1}_MinMaxScaler_{2}_{3}_{4}_{5}_{6}.csv'
                    .format(interval, candle, lower, upper, date, period, go_back),
                    source, interval, candle, date, scaler, lower, upper, period, go_back, )
        scaler = 'RobustScaler'
        calc_profit('./out/basson_us_2sigs_rf/predictions_iex_{0}_{1}_RobustScaler_{2}_{3}_{4}_{5}_{6}.csv'
                    .format(interval, candle, lower, upper, date, period, go_back),
                    source, interval, candle, date, scaler, lower, upper, period, go_back, )


def potential_profit(x, **kwargs):
    if (x == -2.0) or (x == 2.0):
        return kwargs['end'] - kwargs['start']
    else:
        return 0.0


def invest(symbol, accuracy, signal):
    return_val = 0.0
    weight_str = weights['Sharpe Portfolio'][symbol+' Weight']
    weight = float(weight_str[:-1])
    # if accuracy >= 0.8:
    #     if signal == -2.0:
    #         return_val = -500.0
    #     elif signal == 2.0:
    #         return_val = 500.0
    if signal < 0.0:
        return_val = -(100000.0 * weight / 100.0)
    elif signal > 0.0:
        return_val = 100000.0 * weight / 100.0
    return return_val


def calc_profit(predictions, source, interval, candle, date, scaler=None,
                lower=0.25, upper=0.75, period=22, go_back=0, ):
    df = pd.read_csv(predictions)
    df['potential_profit'] = np.subtract(df['back-test close'], df['close'])
    df['potential_profit_%'] = (df['potential_profit'] / df['close']) * 100.0
    df['investment'] = df[['symbol', 'accuracy', 'prediction']].apply(lambda x: invest(*x), axis=1)
    df['invt_profit'] = (df['potential_profit_%'] * df['investment']) / 100
    df[['back-test close', 'back-test date', 'accuracy', 'candle', 'close', 'date', 'features', 'interval', 'precision',
        'prediction', 'recall', 'shape', 'signal', 'source', 'symbol', 'potential_profit', 'potential_profit_%',
        'investment', 'invt_profit', ]]\
        .to_csv(predictions)
    output = './out/basson_us_2sigs_rf/predictions_iex_{0}_{1}_{2}_{3}_{4}_{5}_{6}_{7}_summary.txt' \
        .format(source, interval, candle, date, scaler, lower, upper, period, go_back, )
    count = df['investment'].astype(bool).sum()
    with open(output, 'w') as file:
        file.write('Profit = ${:,.2f}'.format(df['invt_profit'].sum()))
        file.write(' from ${:,.2f} invested in {} stocks.\n'.format(count * 500, count))
        symbols = df.loc[(df.investment == 500.0) | (df.investment == -500.0) & (df.accuracy >= 0.85), ['symbol', 'signal', ]]
        for index, row in symbols.iterrows():
            file.write('{}, {}\n'.format(row['symbol'], row['signal']))


main()
