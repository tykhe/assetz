# import needed modules
import quandl
import json
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
from assetpredict.data import get_data


# get adjusted closing prices of 5 selected companies with Quandl
# quandl.ApiConfig.api_key = 'zcfJ6696mcZScjzsyeta'
# selected = ['CNP', 'F', 'WMT', 'GE', 'TSLA', 'GOOG', 'AAPL', 'EDIT', 'EBAY', 'DIS', 'MCD',]
portfolio = './in/basson-portfolio-01-US.txt'
selected = np.loadtxt(portfolio, dtype=np.str, )
# data = quandl.get_table('WIKI/PRICES', ticker = selected,
#                         qopts = { 'columns': ['date', 'ticker', 'adj_close'] },
#                         date = { 'gte': '2014-1-1', 'lte': '2016-12-31' }, paginate=True)


us_holidays = ['2018-01-01', '2018-01-15', '2018-02-19', '2018-03-30', '2018-05-28',
               '2018-07-04', '2018-09-03', '2018-11-22', '2018-12-25']
us_weekmask = '1111100'
go_back = np.busday_count('2018-06-29', '2018-11-25', weekmask=us_weekmask, holidays=us_holidays, )

# go_back = 89
data = pd.DataFrame()
for ticker in selected:
    ticker_data = get_data(ticker)
    ticker_data = ticker_data[:-go_back]
    ticker_data['ticker'] = ticker
    subset = ticker_data[['date', 'ticker', 'close']]
    data = data.append(ticker_data[['date', 'ticker', 'close']])
data = data.rename(columns={'close': 'adj_close'})

# reorganise data pulled by setting date as index with
# columns of tickers and their corresponding adjusted prices
clean = data.set_index('date')
table = clean.pivot(columns='ticker')

# calculate daily and annual returns of the stocks
returns_daily = table.pct_change()
returns_annual = returns_daily.mean() * 250

# get daily and covariance of returns of the stock
cov_daily = returns_daily.cov()
cov_annual = cov_daily * 250

# empty lists to store returns, volatility and weights of imiginary portfolios
port_returns = []
port_volatility = []
sharpe_ratio = []
stock_weights = []

# set the number of combinations for imaginary portfolios
num_assets = len(selected)
num_portfolios = 50000

#set random seed for reproduction's sake
np.random.seed(101)

# populate the empty lists with each portfolio's returns,risk and weights
for single_portfolio in range(num_portfolios):
    weights = np.random.random(num_assets)
    weights /= np.sum(weights)
    returns = np.dot(weights, returns_annual)
    volatility = np.sqrt(np.dot(weights.T, np.dot(cov_annual, weights)))
    sharpe = returns / volatility
    sharpe_ratio.append(sharpe)
    port_returns.append(returns)
    port_volatility.append(volatility)
    stock_weights.append(weights)

# a dictionary for Returns and Risk values of each portfolio
portfolio = {'Returns': port_returns,
             'Volatility': port_volatility,
             'Sharpe Ratio': sharpe_ratio}

# extend original dictionary to accomodate each ticker and weight in the portfolio
for counter,symbol in enumerate(selected):
    portfolio[symbol+' Weight'] = [Weight[counter] for Weight in stock_weights]

# make a nice dataframe of the extended dictionary
df = pd.DataFrame(portfolio)

# get better labels for desired arrangement of columns
column_order = ['Returns', 'Volatility', 'Sharpe Ratio'] + [stock+' Weight' for stock in selected]

# reorder dataframe columns
df = df[column_order]

# find min Volatility & max sharpe values in the dataframe (df)
min_volatility = df['Volatility'].min()
max_sharpe = df['Sharpe Ratio'].max()

# use the min, max values to locate and create the two special portfolios
sharpe_portfolio = df.loc[df['Sharpe Ratio'] == max_sharpe]
min_variance_port = df.loc[df['Volatility'] == min_volatility]

# plot frontier, max sharpe & min Volatility values with a scatterplot
plt.style.use('seaborn-dark')
plt.xlabel('Volatility (Std. Deviation)')
plt.ylabel('Expected Returns')
plt.title('Efficient Frontier')
df.plot.scatter(x='Volatility', y='Returns', c='Sharpe Ratio',
                cmap='RdYlGn', edgecolors='black', figsize=(10, 8), grid=True)
plt.scatter(x=sharpe_portfolio['Volatility'], y=sharpe_portfolio['Returns'], c='red', marker='D', s=200)
fig = plt.scatter(x=min_variance_port['Volatility'], y=min_variance_port['Returns'], c='blue', marker='D', s=200 ).get_figure()
# plt.show()
pdf = PdfPages('out/basson_us_2sigs_rf/markowitz-efficient-frontier.pdf')
pdf.savefig(fig)
pdf.close()
plt.close()

# print the details of the 2 special portfolios
print(min_variance_port.T)
print(sharpe_portfolio.T)

portfolio = {'Portfolio': {}}
portfolio['Portfolio'] = {'Name': 'Exemplar Markowitz Efficient Frontier weighted portfolio',
                          'Date': '2018-06-29'}
mVP = {}
cols = list(min_variance_port.columns.values)
for index, row in min_variance_port.iterrows():
    for col in cols:
        if col == 'Sharpe Ratio':
            mVP[col] = f'{row[col]:.2f}'
        else:
            mVP[col] = f'{(row[col] * 100):.2f}%'
portfolio['Minimum Variance Portfolio'] = mVP

sP = {}
for index, row in sharpe_portfolio.iterrows():
    for col in cols:
        if col == 'Sharpe Ratio':
            sP[col] = f'{row[col]:.2f}'
        else:
            sP[col] = f'{(row[col] * 100):.2f}%'
portfolio['Sharpe Portfolio'] = sP
portfolio_json = json.dumps(portfolio, indent=4)
with open('out/basson_us_2sigs_rf/portfolio.json', 'w') as f:
    f.write(portfolio_json)
    f.close()

min_variance_port = min_variance_port * 100
min_variance_port['Sharpe Ratio'] = min_variance_port['Sharpe Ratio'] / 100
sharpe_portfolio = sharpe_portfolio * 100
sharpe_portfolio['Sharpe Ratio'] = sharpe_portfolio['Sharpe Ratio'] / 100
with open('out/basson_us_2sigs_rf/portfolio.txt', 'w') as f:
    f.write('Minimum variance portfolio:\n')
    f.write(min_variance_port.T.to_string(header=False, float_format='%.2f'))
    f.write('\n\nSharpe portfolio\n')
    f.write(sharpe_portfolio.T.to_string(header=False, float_format='%.2f'))
    f.close()

min_variance_port.T.to_csv('out/basson_us_2sigs_rf/min-variance-portfolio.csv', header=False)
sharpe_portfolio.T.to_csv('out/basson_us_2sigs_rf/sharpe-portfolio.csv', header=False)

