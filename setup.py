from setuptools import setup

setup(
    name="assetz",
    version="0.0.2",
    description="Random Forest Asset Price Prediction",
    packages=['assetz', 'assetz.loader',],
    install_requires=[
        'scikit-learn==0.23.2',
        'requests==2.24',
        'pandas==1.1.4',
        'click==7.12',
        'scipy==1.5.4',
        'stockstats==0.3.2',
        'numpy==1.19.4',
        'ccxt==1.37.30',
        'binance==0.7.5',
        'fbprophet==0.7.1',
        'Quandl==3.5.3',
        'matplotlib==3.3.2',
        'pystan==2.19.1.1',
        'pytrends==4.7.3',
    ],
    extras_require={
        "dev": [
            "ipython==7.19.0",
        ]
    }
)