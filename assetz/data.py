from assetz import settings
from assetz import ti
from assetz.loader.iex import stock_data_pd as iex_stock_data_pd
from assetz.loader.shrimpy import stock_data_pd as shrimpy_stock_data_pd
# from assetz.loader.src_binance import stock_data_pd as bin_stock_data_pd
from stockstats import StockDataFrame
from assetz.settings import SHRIMPY_DEV_API_KEY, SHRIMPY_DEV_SECRET_KEY
import shrimpy

client = shrimpy.ShrimpyApiClient(SHRIMPY_DEV_API_KEY, SHRIMPY_DEV_SECRET_KEY)


def get_crypto_exchanges():
    supported_exchanges = client.get_supported_exchanges()
    crypto_exchanges = []
    for item in supported_exchanges:
        crypto_exchanges.append(item['exchange'])
    return crypto_exchanges


def get_data(symbol, source='binance', interval='5y', candle='1d', offline=False):
    if source == 'iex':
        data = iex_stock_data_pd(symbol, interval, offline)
    elif source in get_crypto_exchanges():
        data = shrimpy_stock_data_pd(source, symbol, interval, candle, offline,)
    # elif source == 'binance':
    #     data = bin_stock_data_pd(symbol, interval, candle, offline)
    return data


def smooth_data(data, smoothing=12):
    return data.ewm(ignore_na=False, span=smoothing, adjust=True, min_periods=0).mean()


def add_indicators(df, indicators=None):
    if not indicators:
        indicators = settings.DEFAULT_INDICATORS

    for indicator in indicators:
        source, ind = indicator.split(':')
        if source == 'ti':
            name, *params = ind.split('_')
            df = getattr(ti, name)(df, *[int(p) for p in params])

    stock_data = StockDataFrame.retype(df)
    for indicator in indicators:
        source, ind = indicator.split(':')
        if source == 'stocksdata':
            stock_data[ind]

    return stock_data
