# import requests
import json
import os
import gzip
import pandas as pd
import shrimpy
from assetz.settings import CACHE_DIR
from assetz.settings import SHRIMPY_DEV_API_KEY, SHRIMPY_DEV_SECRET_KEY

client = shrimpy.ShrimpyApiClient(SHRIMPY_DEV_API_KEY, SHRIMPY_DEV_SECRET_KEY)


def check_newer(exchange, symbol, interval, candle, last_date):
    # data = requests.get(f'https://api.iextrading.com/1.0/stock/{symbol}/chart/{interval}').json()
    data = client.get_candles(exchange=exchange,
                              base_trading_symbol=symbol,
                              quote_trading_symbol='BTC',
                              interval=candle,
                              start_time=last_date,
                              )

    # return new data (first row overlaps with previous fetch)
    return False, data[1:]

    # if data[-1]['date'] != last_date:
    #     new_items = []
    #
    #     for item in reversed(data):
    #         if item['date'] == last_date:
    #             break
    #
    #         new_items.append(item)
    #
    #     return False, list(reversed(new_items))
    #
    # return True, []


def stock_data_source(exchange, symbol, interval='5y', candle='1d', offline=False):
    assert candle in ['1m', '5m', '15m', '1h', '6h', '1d',]

    symbol = symbol.upper()

    file_path = os.path.join(CACHE_DIR, f'{exchange}/candles-{candle}-{symbol}.json.gz')
    if os.path.isfile(file_path):
        with gzip.open(file_path, 'rb') as file:
            data = json.loads(file.read().decode('utf-8'))
            if not offline:
                err, new_data = check_newer(exchange, symbol, interval, candle, data[-1]['time'])
                if not err:
                    data.extend(new_data)
                    with gzip.GzipFile(file_path, 'w') as file_write:
                        file_write.write(json.dumps(data).encode('UTF-8'))
            return data
    else:
        # data = requests.get(f'https://api.iextrading.com/1.0/stock/{symbol}/chart/{interval}').json()
        data = client.get_candles(exchange=exchange,
                                  base_trading_symbol=symbol,
                                  quote_trading_symbol='BTC',
                                  interval=candle,
                                  )
        with gzip.GzipFile(file_path, 'w') as file:
            file.write(json.dumps(data).encode('UTF-8'))
            file.close()
        return data


def stock_data_pd(exchange, symbol, interval='5y', candle='1d', offline=False):
    return pd.DataFrame(stock_data_source(exchange, symbol, interval, candle, offline))
