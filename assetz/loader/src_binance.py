import time
import dateparser
import pytz
import ccxt
import requests
import json
import os
import gzip
import pandas as pd
from assetz.settings import CACHE_DIR
from datetime import datetime, timedelta
from binance.client import Client


def main():
    df = get_symbols()
    symbols = df['symbols']
    for symbol in symbols:
        print(stock_data_pd(symbol, '5y', '6h',).shape)


def get_symbols():
    symbols = []
    data = requests.get(f'https://api.binance.com/api/v1/exchangeInfo').json()
    for symbol in data['symbols']:
        symbols.append(symbol['symbol'])
    # print(symbols)
    df = pd.DataFrame(symbols, columns=('symbols', ))
    df.to_csv('./data_cache/binance/markets.csv')
    return df


def normalise_data(raw_klines):
    '''
    Binance API returns a list of list of values, but we need a list of dictionaries for downstream processing.
    :param raw_klines: the list of lists returned through CCXT API (Binance API).
    :return: a list of dictionaries
    '''
    data = []
    for row in raw_klines:
        d = {}
        close_date = datetime.utcfromtimestamp(row[6] / 1000).strftime('%Y-%m-%d')
        label = datetime.utcfromtimestamp(row[6] / 1000).strftime('%b %d, %y')
        d['date'] = close_date
        d['open'] = float(row[1])
        d['high'] = float(row[2])
        d['low'] = float(row[3])
        d['close'] = float(row[4])
        d['volume'] = float(row[5])
        d['unadjustedVolume'] = float(row[5])
        if len(data) > 0:
            d['change'] = d['close'] - data[-1]['close']
            d['changePercent'] = d['change'] / data[-1]['close'] * 100
            d['changeOverTime'] = (d['close'] - data[0]['close']) / data[0]['close']
        else: # first row of data, so change == 0
            d['change'] = 0.0
            d['changePercent'] = 0.0
            d['changeOverTime'] = 0.0
        d['label'] = label
        data.append(d)
    return data


def check_newer(symbol, interval, last_date):
    # data = requests.get(f'https://api.iextrading.com/1.0/stock/{symbol}/chart/{interval}').json()
    # data = requests.get(f'https://api.binance.com/api/v1/exchangeInfo').json()
    raw_klines = get_klines(symbol, Client.KLINE_INTERVAL_1DAY, last_date, )
    data = normalise_data(raw_klines)
    if data[-1]['date'] != last_date:
        new_items = []

        for item in reversed(data):
            if item['date'] == last_date:
                break

            new_items.append(item)

        return False, list(reversed(new_items))

    return True, []


def stock_data_source(symbol, interval='5y', candle='1d', offline=False):
    binance = ccxt.binance()
    assert interval in ['5y', '4y', '3y', '2y', '1y', '6m', '3m', '1m', '14d', '7d',]
    now = datetime.now()
    start_str = (now - timedelta(days=1825)).strftime('%d %b, %Y')
    print(start_str)
    candles = binance.timeframes
    assert candle in candles
    symbol = symbol.upper()

    file_path = os.path.join(CACHE_DIR, f'binance/chart-{interval}-{candle}-{symbol}.json.gz')
    if os.path.isfile(file_path):
        with gzip.open(file_path, 'rb') as file:
            data = json.loads(file.read().decode('utf-8'))
            if not offline:
                err, new_data = check_newer(symbol, interval, data[-1]['date'])
                if not err:
                    data.extend(new_data)
                    with gzip.GzipFile(file_path, 'w') as file_write:
                        file_write.write(json.dumps(data).encode('UTF-8'))
            return data
    else:
        # data = requests.get(f'https://api.iextrading.com/1.0/stock/{symbol}/chart/{interval}').json()
        raw_klines = get_klines(symbol, Client.KLINE_INTERVAL_1DAY, start_str, )
        data = normalise_data(raw_klines)
        with gzip.GzipFile(file_path, 'w') as file:
            file.write(json.dumps(data).encode('UTF-8'))
        return data


def stock_data_pd(symbol, interval='5y', candle='1d', offline=False):
    return pd.DataFrame(stock_data_source(symbol, interval, candle, offline))


def date_to_milliseconds(date_str):
    """Convert UTC date to milliseconds

    If using offset strings add "UTC" to date string e.g. "now UTC", "11 hours ago UTC"

    See dateparse docs for formats http://dateparser.readthedocs.io/en/latest/

    :param date_str: date in readable format, i.e. "January 01, 2018", "11 hours ago UTC", "now UTC"
    :type date_str: str
    """
    # get epoch value in UTC
    epoch = datetime.utcfromtimestamp(0).replace(tzinfo=pytz.utc)
    # parse our date string
    d = dateparser.parse(date_str)
    # if the date is not timezone aware apply UTC timezone
    if d.tzinfo is None or d.tzinfo.utcoffset(d) is None:
        d = d.replace(tzinfo=pytz.utc)

    # return the difference in time
    return int((d - epoch).total_seconds() * 1000.0)


def interval_to_milliseconds(interval):
    """Convert a Binance interval string to milliseconds

    :param interval: Binance interval string 1m, 3m, 5m, 15m, 30m, 1h, 2h, 4h, 6h, 8h, 12h, 1d, 3d, 1w
    :type interval: str

    :return:
         None if unit not one of m, h, d or w
         None if string not in correct format
         int value of interval in milliseconds
    """
    ms = None
    seconds_per_unit = {
        "m": 60,
        "h": 60 * 60,
        "d": 24 * 60 * 60,
        "w": 7 * 24 * 60 * 60
    }

    unit = interval[-1]
    if unit in seconds_per_unit:
        try:
            ms = int(interval[:-1]) * seconds_per_unit[unit] * 1000
        except ValueError:
            pass
    return ms


def get_klines(symbol, interval, start_str, end_str=None, limit=500,):
    """Get Historical Klines from Binance

    See dateparse docs for valid start and end string formats http://dateparser.readthedocs.io/en/latest/

    If using offset strings for dates add "UTC" to date string e.g. "now UTC", "11 hours ago UTC"

    :param symbol: Name of symbol pair e.g BNBBTC
    :type symbol: str
    :param interval: Biannce Kline candle size
    :type interval: str
    :param start_str: Start date string in UTC format
    :type start_str: str
    :param end_str: optional - end date string in UTC format
    :type end_str: str
    :param limit: the maximum rows to fetch (default = 500)
    :type limit: int
    :return: list of list of OHLCV values
    """
    # create the Binance client, no need for api key
    client = Client("", "")

    # init our list
    output_data = []

    # setup the max limit
    # limit = 500

    # convert interval to useful value in seconds
    timeframe = interval_to_milliseconds(interval)

    # convert our date strings to milliseconds
    start_ts = date_to_milliseconds(start_str)

    # if an end time was passed convert it
    end_ts = None
    if end_str:
        end_ts = date_to_milliseconds(end_str)

    idx = 0
    # it can be difficult to know when a symbol was listed on Binance so allow start time to be before list date
    symbol_existed = False
    while True:
        # fetch the klines from start_ts up to max 500 entries or the end_ts if set
        temp_data = client.get_klines(
            symbol=symbol,
            interval=interval,
            limit=limit,
            startTime=start_ts,
            endTime=end_ts
        )

        # handle the case where our start date is before the symbol pair listed on Binance
        if not symbol_existed and len(temp_data):
            symbol_existed = True

        if symbol_existed:
            # append this loops data to our output data
            output_data += temp_data

            # update our start timestamp using the last value in the array and add the interval timeframe
            start_ts = temp_data[len(temp_data) - 1][0] + timeframe
        else:
            # it wasn't listed yet, increment our start date
            start_ts += timeframe

        idx += 1
        # check if we received less than the required limit and exit the loop
        if len(temp_data) < limit:
            # exit the while loop
            break

        # sleep after every 3rd call to be kind to the API
        if idx % 3 == 0:
            time.sleep(1)

    return output_data


# main()