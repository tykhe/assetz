import requests
import json
import os
import gzip
import pandas as pd
from assetz.settings import CACHE_DIR


def check_newer(symbol, interval, last_date):
    data = requests.get(f'https://api.iextrading.com/1.0/stock/{symbol}/chart/{interval}').json()

    if data[-1]['date'] != last_date:
        new_items = []

        for item in reversed(data):
            if item['date'] == last_date:
                break

            new_items.append(item)

        return False, list(reversed(new_items))

    return True, []


def stock_data_source(symbol, interval='5y', offline=False):
    assert interval in ['5y', '1y']
    symbol = symbol.upper()

    file_path = os.path.join(CACHE_DIR, f'iex/chart-{interval}-{symbol}.json.gz')
    if os.path.isfile(file_path):
        with gzip.open(file_path, 'rb') as file:
            data = json.loads(file.read().decode('utf-8'))
            if not offline:
                err, new_data = check_newer(symbol, interval, data[-1]['date'])
                if not err:
                    data.extend(new_data)
                    with gzip.GzipFile(file_path, 'w') as file_write:
                        file_write.write(json.dumps(data).encode('UTF-8'))
            return data
    else:
        data = requests.get(f'https://api.iextrading.com/1.0/stock/{symbol}/chart/{interval}').json()
        with gzip.GzipFile(file_path, 'w') as file:
            file.write(json.dumps(data).encode('UTF-8'))
        return data


def stock_data_pd(symbol, interval='5y', candle='1d', offline=False):
    return pd.DataFrame(stock_data_source(symbol, interval, offline))
