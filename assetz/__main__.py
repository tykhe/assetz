import click
from assetz.randomforest import process


@click.command()
@click.argument('symbol')
@click.option('--source', default='binance', help="Data source")
@click.option('--candle', default='1d', help="Candle period")
@click.option('--period', default=90, help="Prediction period")
@click.option('--go_back', default=0, help="Go back in time (N candlesticks)")
@click.option('--offline', is_flag=True, help="Offline mode")
@click.option('--verbose', is_flag=True, help="Display more info")
def run(symbol, period, go_back, offline, verbose, source='binance', candle='1d',):
    assert period > 0
    assert go_back >= 0

    err, data = process(symbol.upper(), source.lower(), candle=candle, period=period, go_back=go_back, offline=offline)
    if err:
        click.echo(f'Error: {data}')
    else:
        click.echo(f"Using source {source}")
        click.echo(f"{data['symbol']}: {data['signal']}")
        if go_back:
            click.echo(f"Going back {go_back} candlesticks")
        click.echo(f"Close price ({data['date']}): {data['close']}")
        if verbose:
            click.echo(f"Shape: {data['shape']}")
            click.echo(f"Accuracy: {data['accuracy']}")
            click.echo(f"Precision: {data['precision']}")
            click.echo(f"Recall: {data['recall']}")
            for feature, val in data['features'].items():
                click.echo(f"Feature importance: {feature}: {val}")


run()
