import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split, cross_val_score
from sklearn.ensemble import RandomForestClassifier, GradientBoostingClassifier
from sklearn.metrics import accuracy_score, precision_score, recall_score
from sklearn.preprocessing import MinMaxScaler, RobustScaler
# import autosklearn.classifier
# import autosklearn.classification
from assetz.data import get_data, smooth_data, add_indicators
from assetz import settings
import math

pd.options.mode.chained_assignment = None


def map_labels(y, lo_q, hi_q, hold_lo_q, hold_hi_q, min, max, mean, std,):
    """
    Takes a normalised scale and maps values to four signals based on the lower and upper quantiles.
    :param y: the normalised value to map to a signal (signal is one of -2.0, -1.0, 1.0, 2.0).
    :param lo_q: the lower quantile used in the mapping.
    :param hi_q: the upper quantile used in the mapping.
    :return: a mapped signal (one of -2.0, -1.0, 1.0, 2.0).
    """

    # Uncomment this section for 2-signal mapping (-1 == Sell, 1 == Buy)
    # if y <= 0.0:
    #     return -1
    # else:
    #     return 1

    # Uncomment this section for 3-signal mapping (-1 == Sell, 0 == Hold, 1 == Buy)
    # if y < 0.0:
    #     return -1
    # elif hold_lo_q <= y <= hold_hi_q:
    #     return 0
    # else:
    #     return 1

    # Uncomment this section for 4-signal mapping (-2 == Strong sell, -1 == Sell, 1 == Buy, 2 == Strong Buy)
    # if y <= lo_q:
    #     return -2
    # elif lo_q < y < 0.0:
    #     return -1
    # elif 0.0 < y <= hi_q:
    #     return 1
    # else:
    #     return 2

    # Uncomment this section for 5-signal mapping (-2 == Strong sell, -1 == Sell, 0 == Hold, 1 == Buy, 2 == Strong Buy)
    if y <= lo_q:
        return -2
    elif lo_q < y < hold_lo_q:
        return -1
    elif hold_lo_q <= y <= hold_hi_q:
        return 0
    elif hold_hi_q < y <= hi_q:
        return 1
    else:
        return 2


def add_friction(y, friction=5.0):
    """
    Adds friction to y defined as the cost of trading (commissions, spreads, errors, and slippage)
    :param y: the profit before friction %
    :param friction: the user-defined amount of friction % (default = 5.0%)
    :return: the friction-adjusted profit %
    """
    y = y - (friction / 100.0)
    return y


def scale_labels(y, scaler_method='MinMaxScaler', low=0.25, high=0.75):
    """
    Normalise labels to provide a consistent set for training and test across the whole portfolio.
    :param y: the raw labels before scaling.
    :param scaler_method: one of None, MinMaxScaler, or RobustScaler.
    :param low: the low quantile cut-off for signalling 'sell strong'.
    :param high: the high quantile cut-off for signalling 'buy strong'.
    :return: a matrix of normalised, scaled labels.
    """
    y = y.reshape(-1, 1)
    scaler = None
    if scaler_method == None:
        y_scaled = y
    elif scaler_method == 'MinMaxScaler':
        scaler = MinMaxScaler((-1, 1), False)
        y_scaled = scaler.fit_transform(y)
    elif scaler_method == 'RobustScaler':
        scaler = RobustScaler(quantile_range=(0.0, 100.0))
        y_scaled = scaler.fit_transform(y)
    df = pd.DataFrame(y_scaled)[0]
    low_q = df.quantile(low)
    high_q = df.quantile(high)
    # Next two quantiles used to determine a 'hold' signal
    hold_low_q = df.quantile(0.4)
    hold_hi_q = df.quantile(0.6)
    min = df.min()
    max = df.max()
    mean = df.mean()
    std = df.std()
    df['label'] = df.apply(map_labels, args=(low_q, high_q, hold_low_q, hold_hi_q, min, max, mean, std),)
    return df['label'].as_matrix()


def process(symbol, source='binance', interval='5y', candle='1d', period=22, go_back=0,
            lower=0.25, upper=0.75, scaler=None, friction=5.0, optimise=False, offline=False):
    """
    Predict a future signal for the given asset symbol.
    :param symbol:
    :param source:
    :param interval
    :param candle:
    :param period:
    :param go_back:
    :param lower:
    :param upper:
    :param scaler:
    :param friction:
    :param optimise: if True, use auto-sklearn to optimise hyperparameters (takes a LONG time, maybe an hour)
    :param offline:
    :return:
    """
    try:
        data = {
            'symbol': symbol,
            'source': source,
            'interval': interval,
            'candle': candle,
            'features': {}
        }

        original_data = get_data(symbol, source, interval, candle, offline)
        # If go_back > 0, travel back in time 'go_back' ticks and then add 'period' ticks of 'future' data
        # to enable back-testing by trimming the original data back to (-go_back+period)
        if go_back:
            working_data = original_data[: -go_back + period]
        else:
            working_data = original_data
        unfiltered_features = add_indicators(smooth_data(working_data))
        # unfiltered_features = add_indicators(smooth_data(original_data))
        unfiltered_features.fillna(0, inplace=True)
        features = unfiltered_features.filter(settings.PREDICT_FEATURES, axis=1)

        if features.shape[0] < 100:
            # missing_items.append(symbol)
            return True, 'Not enough data'

        # This section calculates the profit % with trading friction included (e.g. slippage, costs, ...)
        labels = np.array(working_data['close'])
        # y0 holds close prices for market entry
        y0 = labels[:features.shape[0] - period]
        # y1 holds close prices for market exit, hence y1-y0 = profit/loss
        y1 = labels[period:]
        # Convert price differential into %
        y = (((y1 - y0) / y0) * 100)
        y = np.apply_along_axis(add_friction, 1, y.reshape(-1, 1), friction, )
        y = y.reshape(-1)
        y = scale_labels(y, scaler, lower, upper, )

        # REPLACED WITH ABOVE CODE Uncomment this section to enable binary classification (buy, sell)
        # labels = np.array(working_data['close'])
        # # Create series of labels (-1 means price increased 'period' ticks after y0, +1 means price decreased)
        # y0 = labels[:features.shape[0] - period]
        # y1 = labels[period:]
        # y = np.sign(y1 - y0)
        # # Convert zeroes (no price change) into -1 (sell signal)
        # y[y == 0] = -1

        unique, counts = np.unique(y, return_counts=True)

        feature_list = list(features.columns)

        # ToDo - investigate 'random shuffle' parameter in train_test_split function
        if go_back:
            train_features, test_features, train_labels, test_labels = train_test_split(
                features[:features.shape[0] - period],
                y,
                # y_scaled,
                **settings.SPLIT_PARAMS,
                shuffle=False,
            )
        else:
            train_features, test_features, train_labels, test_labels = train_test_split(
                features[:features.shape[0] - period],
                y,
                # y_scaled,
                **settings.SPLIT_PARAMS,
                shuffle=False,
            )

        data['shape'] = features.shape

        if optimise:
            pass
            # rf = autosklearn.classification.AutoSklearnClassifier(**settings.OPTIMISE_PARAMS)
        else:
            # rf = GradientBoostingClassifier(**settings.GB_CLASSIFIER_PARAMS)
            rf = RandomForestClassifier(**settings.RF_CLASSIFIER_PARAMS)

        # scores = cross_val_score(rf, train_features, train_labels, cv=10)
        # for i, score in enumerate(scores):
        #     data['score_{}'.format(i)] = score

        rf.fit(train_features, train_labels)
        predictions = rf.predict(test_features)
        data['accuracy'] = accuracy_score(test_labels, predictions)
        data['precision'] = precision_score(test_labels, predictions, average=None)
        data['recall'] = recall_score(test_labels, predictions, average=None)
        # data['precision'] = precision_score(test_labels, predictions, average='binary')
        # data['recall'] = recall_score(test_labels, predictions, average='binary')
        # data['precision'] = precision_score(test_labels, predictions, average='micro')
        # data['recall'] = recall_score(test_labels, predictions, average='micro')

        if not optimise:
            importances = list(rf.feature_importances_)
            feature_importances = [(feature, round(importance, 2)) for feature, importance in
                                   zip(feature_list, importances)]
            feature_importances = sorted(feature_importances, key=lambda x: x[1], reverse=True)
            for feature, importance in feature_importances:
                data['features'][feature] = importance

        if go_back:
            # We are trying to predict the last 'period' ticks
            future_pred = rf.predict(features[:-period])
            data['back-test date'] = features.index[-1 - period]
            # 'labels' has already been adjusted for 'go_back'
            data['back-test close'] = labels[-1 - period]
            # data['(-1) prediction'] = future_pred[-1]
        else:
            future_pred = rf.predict(features)

        data['prediction'] = future_pred[-1]
        data['close'] = labels[-1]
        data['date'] = working_data['date'].iloc[-1]

        # Uncomment this section for binary classification with two signals (1 == 'Buy', -1 == 'Sell')
        # if go_back:
        #     # Convenience output column to show if back-tested prediction outcome was correct (1) or incorrect (-1)
        #     if future_pred[-1] == 1:
        #         data['__outcome'] = np.sign(data['close'] - data['back-test close'])
        #     elif future_pred[-1] == -1:
        #         data['__outcome'] = np.sign(data['back-test close'] - data['close'])

        # Uncomment this section for tertiary classification (1 == 'Buy', 0 == 'Hold', -1 == 'Sell')')
        # if go_back:
        #     # Convenience output column to show if back-tested prediction outcome was correct (1) or incorrect (-1)
        #     if future_pred[-1] == 1:
        #         data['__outcome'] = np.sign(data['close'] - data['back-test close'])
        #     elif future_pred[-1] == 0:
        #         x = (data['close'] - data['back-test close']) / data['close']
        #         # x = sqrt (x * x)
        #         if math.fabs(x) <= 0.02:
        #             data['__outcome'] = 1
        #         else:
        #             data['__outcome'] = -1
        #     else:
        #         data['__outcome'] = np.sign(data['back-test close'] - data['close'])

        # Uncomment this section for 4-signal classification (2 == 'Strong buy', 1 == 'Buy', -1 == 'Sell', -2 == 'Strong sell')
        # if go_back:
        #     # Convenience output column to show if back-tested prediction outcome was correct (1) or incorrect (-1)
        #     if future_pred[-1] > 0:
        #         data['__outcome'] = np.sign(data['close'] - data['back-test close'])
        #     elif future_pred[-1] <= 0:
        #         data['__outcome'] = np.sign(data['back-test close'] - data['close'])

        # Uncomment this section for 5-signal classification (2 == 'Strong buy', 1 == 'Buy', 0 == 'Hold', -1 == 'Sell', -2 == 'Strong sell')
        if go_back:
            # Convenience output column to show if back-tested prediction outcome was correct (1) or incorrect (-1)
            if future_pred[-1] > 0:
                data['__outcome'] = np.sign(data['close'] - data['back-test close'])
            elif future_pred[-1] == 0:
                x = (data['close'] - data['back-test close']) / data['close']
                # x = sqrt (x * x)
                if math.fabs(x) <= 0.02:
                    data['__outcome'] = 1
                else:
                    data['__outcome'] = -1
            else:
                data['__outcome'] = np.sign(data['back-test close'] - data['close'])

        # Uncomment this section for binary classification (1 == 'Buy', -1 == 'Sell')')
        # if future_pred[-1] == 1:
        #     data['signal'] = 'Buy'
        # elif future_pred[-1] == -1:
        #     data['signal'] = 'Sell'

        # Uncomment this section for tertiary classification (1 == 'Buy', 0 == 'Hold', -1 == 'Sell')')
        # if future_pred[-1] == 1:
        #     data['signal'] = 'Buy'
        # elif future_pred[-1] == 0:
        #     data['signal'] = 'Hold'
        # elif future_pred[-1] == -1:
        #     data['signal'] = 'Sell'

        # Uncomment this section for 4-signal classification (2 == 'Strong buy', 1 == 'Buy', -1 == 'Sell', -2 == 'Strong sell')
        # if future_pred[-1] == 2:
        #     data['signal'] = 'Strong Buy'
        # if future_pred[-1] == 1:
        #     data['signal'] = 'Buy'
        # elif future_pred[-1] == -1:
        #     data['signal'] = 'Sell'
        # elif future_pred[-1] == -2:
        #     data['signal'] = 'Strong Sell'

        # Uncomment this section for 5-signal classification (2 == 'Strong buy', 1 == 'Buy', 0 == 'Hold', -1 == 'Sell', -2 == 'Strong sell')
        if future_pred[-1] == 2:
            data['signal'] = 'Strong Buy'
        if future_pred[-1] == 1:
            data['signal'] = 'Buy'
        elif future_pred[-1] == 0:
            data['signal'] = 'Hold'
        elif future_pred[-1] == -1:
            data['signal'] = 'Sell'
        elif future_pred[-1] == -2:
            data['signal'] = 'Strong Sell'

        return False, data
    except Exception as e:
        print(e)
        return True, e
