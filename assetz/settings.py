import os

CACHE_DIR = os.path.abspath(os.path.join(os.path.basename(__file__), '../../data_cache/'))

DEFAULT_INDICATORS = [
    'stocksdata:macd',
    'stocksdata:rsi_14',
    'stocksdata:wr_14',
    'stocksdata:macd',
    'ti:STO_14',
    'ti:OnBalanceVolume_2',
    'ti:ROC_2'
]

PREDICT_FEATURES = [
    # 'open',
    # 'high',
    # 'low',
    # 'index',
    # 'close',
    'macd',
    'macdh',
    'obv_2',
    'roc_2',
    'rsi_14',
    'volume',
    'wr_14',
    'sod_14'
]

SPLIT_PARAMS = {
    'test_size': 0.25,
    'random_state': 42
}

RF_CLASSIFIER_PARAMS = {
    'n_estimators': 100,
    'criterion': 'gini',
    'random_state': 42
}

GB_CLASSIFIER_PARAMS = {
    'n_estimators': 100,
    'loss': 'deviance',
    'learning_rate': 0.1,
    'max_depth': 3,
    'criterion': 'friedman_mse',
    'min_samples_split': 2,
    'min_samples_leaf': 1,
    'min_weight_fraction_leaf': 0,
    'subsample': 1.0,
    'max_features': None,
    'max_leaf_nodes': None,
    'min_impurity_decrease': 0.0,
    'init': None,
    'verbose': 0,
    'warm_start': False,
    'random_state': 42,
    'presort': 'auto',
}

OPTIMISE_PARAMS = {

}

# Shrimpy Dev
SHRIMPY_BASE_URL = 'https://api.shrimpy.io/'
SHRIMPY_DEV_BASE_URL = 'https://dev-api.shrimpy.io/'
# Master Keys with User & Account permissions
SHRIMPY_DEV_API_KEY = 'a78c0d046170a57edd2e464782e16a6c5c9d837c8e46cfc9dafc352374ef68ad'
SHRIMPY_DEV_SECRET_KEY = 'ad95bdcef493afa507454364f9d9a33a861e62f87109e3351c4e7e3e9c6fa07c2e069f9794d97fe2c7e5a749ba4916c3772c032a3de49c9a1ab07168c1629f0d'
# Master Keys with User, Account & Trade permissions
SHRIMPY_API_MASTER = ''
SHRIMPY_SECRET_MASTER = ''
